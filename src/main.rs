extern crate savefile;

use std::io;
use savefile::prelude::*;
use serde::{Serialize, Deserialize};

// #[macro_use]
// extern crate savefile_derive;

#[derive(Serialize, Deserialize, Debug, Clone)]
enum BinaryTree<T> {
    Empty,
    NonEmpty(Box<TreeNode<T>>)
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct TreeNode<T> {
    element: T,
    left: BinaryTree<T>,
    right: BinaryTree<T>,
}

impl <T: Ord + std::fmt::Display> BinaryTree<T> {
    fn add(&mut self, value: T) {
        match *self {
            BinaryTree::Empty => {
                *self = BinaryTree::NonEmpty(Box::new(TreeNode {
                    element: value,
                    left: BinaryTree::Empty,
                    right: BinaryTree::Empty
                }))
            },
            BinaryTree::NonEmpty(ref mut node) => {
                if value <= node.element {
                    node.left.add(value);
                } else {
                    node.right.add(value);
                }
            }
        }
    }

    fn find(&self, value: T) -> Option<&T> {
        match *self {
            BinaryTree::Empty => {
                println!("{} - not found...", value);
                None
            },
            BinaryTree::NonEmpty(ref node) => {
                if value == node.element {
                    Some(&node.element)
                } else if value <= node.element {
                    node.left.find(value)
                } else {
                    node.right.find(value)
                }
            }
        }
    }

    fn count(&self, side: &str) -> i32 {
        let mut count: i32 = 0;

        match self {
            BinaryTree::Empty => (),
            BinaryTree::NonEmpty(ref node) => {
                count += 1;
                if side == "both" || side ==  "right" {
                    count += node.right.count("both");
                }
                if side == "both" || side ==  "left" {
                    count += node.left.count("both");
                }
            }
        }

        count
    }
}

impl BinaryTree<String> {
    fn load(file_name: &str) -> BinaryTree<String> {
        let file: Result<String, SavefileError> = load_file(&*format!("{}.bin", file_name), 0);
        match file {
            Ok(tree) =>  serde_json::from_str(&*tree).unwrap(),
            _ => BinaryTree::Empty
        }
    }

    fn save(&self) {
        let data = serde_json::to_string(&self).unwrap();
        save_file("btree.bin", 0, &data).unwrap();
    }
}

fn read_args(tree: &mut BinaryTree<String>) {
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_bytes) => {
            input.pop();
            let args: Vec<&str> = input.split(" ").collect();

            if args.len() == 2 {
                match args[0] {
                    "add" => {
                        tree.add(args[1].to_string());

                        println!("Right: {}, left: {}", tree.count("right"), tree.count("left"));
                    },
                    "find" => match tree.find(args[1].to_string()) {
                        Some(v) => println!("Find value: {}", v),
                        None => ()
                    },
                    "count" => {
                        match args[1] {
                            "left" => {
                                println!("Total left side count is: {}", tree.count("left"));
                            },
                            "right" => {
                                println!("Total right side count is: {}", tree.count("right"));
                            },
                            "both" => {
                                println!("Total both sides count is: {}", tree.count("both") );
                            },
                            _ => println!("Second argument is unknown...")
                        }
                    },
                    _ => println!("Unknown command...")
                }

                tree.save();
                // println!("\n {:#?}", tree);
            } else {
                if args[0] == "exit" {
                    return
                } else if args[0] == "print" {
                    println!("\n {:#?}", tree);
                } else {
                    println!("Wrong arguments number...");
                }
            }


            read_args(tree);
        }
        Err(error) => println!("error: {}", error),
    }
}

fn main() {
    let mut  tree: BinaryTree<String> = BinaryTree::load("btree");
    read_args(&mut tree);
}
